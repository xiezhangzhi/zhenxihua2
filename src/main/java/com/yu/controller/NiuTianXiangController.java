package com.yu.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:xiangTian
 * @version:1.0
 * @date:2019/7/23
 * @description:com.yu.controller
 */
@RestController
public class NiuTianXiangController {
    @GetMapping("/niutianxiang")
    public String name(){
        return "牛天翔";
    }
}
