package com.yu.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/lvxiwei")
public class louisController {
    /**
     *@Description 吕习伟的controller
     *@return
     *@Author GodV
     *@Date  2019/7/23
     */
    @GetMapping("/lvxiwei")
    public String name(){
        return "吕习伟";
    }

}
